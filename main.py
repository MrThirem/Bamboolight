import pyrebase
import RPi.GPIO as GPIO


#CONSTANTES
config = {
    "apiKey": "AIzaSyB9ze27jIBSJ6rqlFQvQl7f_VZ-EZl7dxM",
    "authDomain": "bamboolight-f6ffc.firebaseapp.com",
    "databaseURL": "https://bamboolight-f6ffc.firebaseio.com",
    "storageBucket": "bamboolight-f6ffc.appspot.com"
}

firebase = pyrebase.initialize_app(config)
db = firebase.database()


#FONCTIONS


def setup():
    GPIO.cleanup()
    GPIO.setmode(GPIO.BOARD)
    chan_list = [3,5,7,8,10,11,12,13,15,16,18,19,21,22,23,24,26]
    GPIO.setup(chan_list, GPIO.OUT)
    GPIO.setup(chan_list,GPIO.LOW)


    my_stream1 = db.child("button1").stream(stream_handler1)
    my_stream2 = db.child("button2").stream(stream_handler2)
    my_stream3 = db.child("button3").stream(stream_handler3)
    my_stream4 = db.child("button4").stream(stream_handler4)
    my_stream5 = db.child("button5").stream(stream_handler5)
    my_stream6 = db.child("button6").stream(stream_handler6)


def loop():
   # Etat = db.child("button1").get().val()
   # print(Etat)
    pass


def stream_handler1(callback):

    Status = callback["data"]["Status"]
    print(" Status ")
    print(Status)
    if(Status==1):
        #Action b1
        GPIO.output(3, GPIO.HIGH)
        print("Bouton 1 on")
    elif(Status==0):
        #Action b2
        GPIO.output(3, GPIO.LOW)
        print("Bouton 1 off")
def stream_handler2(callback):

    Status = callback["data"]["Status"]
    print(" Status ")
    print(Status)

    if(Status==1):
        GPIO.output(5, GPIO.HIGH)
        print("Bouton 2 on")
    elif(Status==0):
        GPIO.output(5, GPIO.LOW)
        print("Bouton 2 off")
def stream_handler3(callback):

    Status = callback["data"]["Status"]
    print(" Status ")
    print(Status)

    if(Status==1):
        GPIO.output(7, GPIO.HIGH)
        print("Bouton 3 on")
    elif(Status==0):
        GPIO.output(7, GPIO.LOW)
        print("Bouton 3 off")

def stream_handler4(callback):

    Status = callback["data"]["Status"]
    print(" Status ")
    print(Status)
    if(Status<=0):
        GPIO.output(8,GPIO.LOW)
        GPIO.output(10,GPIO.LOW)
        GPIO.output(12,GPIO.LOW)
        print("Bouton 4 off")
    else:
        print("Bouton 4 on")
        if(Status==1):
            GPIO.output(8,GPIO.HIGH)
            GPIO.output(10,GPIO.LOW)
            GPIO.output(12,GPIO.LOW)
            print("Bouton 4 ROUGE")
        elif(Status==2):
            GPIO.output(10,GPIO.HIGH)
            GPIO.output(8,GPIO.LOW)
            GPIO.output(12,GPIO.LOW)
            print("Bouton 4 VERT")
        elif(Status==3):
            GPIO.output(12,GPIO.HIGH)
            GPIO.output(8,GPIO.LOW)
            GPIO.output(10,GPIO.LOW)
            print("Bouton 4 BLEU")

def stream_handler5(callback):
    Status = callback["data"]["Status"]
    print(" Status ")
    print(Status)
    if(Status<=0):
        GPIO.output(11,GPIO.LOW)
        GPIO.output(13,GPIO.LOW)
        GPIO.output(15,GPIO.LOW)
        print("Bouton 5 off")
    else:
        print("Bouton 5 on")
        if(Status==1):
            GPIO.output(11,GPIO.HIGH)
            GPIO.output(13,GPIO.LOW)
            GPIO.output(15,GPIO.LOW)
            print("Bouton 5 ROUGE")
        elif(Status==2):
            GPIO.output(13,GPIO.HIGH)
            GPIO.output(11,GPIO.LOW)
            GPIO.output(15,GPIO.LOW)
            print("Bouton 5 VERT")
        elif(Status==3):
            GPIO.output(15,GPIO.HIGH)
            GPIO.output(13,GPIO.LOW)
            GPIO.output(11,GPIO.LOW)
            print("Bouton 5 BLEU")

def stream_handler6(callback):
    Status = callback["data"]["Status"]
    print(" Status ")
    print(Status)
    if(Status<=0):
        GPIO.output(22,GPIO.LOW)
        GPIO.output(24,GPIO.LOW)
        GPIO.output(26,GPIO.LOW)
        print("Bouton 6 off")
    else:
        print("Bouton 6 on")
        if(Status==1):
            GPIO.output(22,GPIO.HIGH)
            GPIO.output(24,GPIO.LOW)
            GPIO.output(26,GPIO.LOW)
            print("Bouton 6 ROUGE")
        elif(Status==2):
            GPIO.output(24,GPIO.HIGH)
            GPIO.output(22,GPIO.LOW)
            GPIO.output(26,GPIO.LOW)
            print("Bouton 6 VERT")
        elif(Status==3):
            GPIO.output(26,GPIO.HIGH)
            GPIO.output(22,GPIO.LOW)
            GPIO.output(24,GPIO.LOW)
            print("Bouton 6 BLEU")

if (__name__ == '__main__'):
    setup()
    while True:
        loop()


